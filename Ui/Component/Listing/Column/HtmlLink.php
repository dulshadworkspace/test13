<?php
/**
 * A Magento 2 module named Experius/ProductFrontendLinkInBackend
 * Copyright (C) 2019 Experius
 *
 * This file is part of Experius/ProductFrontendLinkInBackend.
 *
 * Experius/ProductFrontendLinkInBackend is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Experius\ProductFrontendLinkInBackend\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class HTMLLink
 * @package Experius\ProductFrontendLinkInBackend\Ui\Component\Listing\Column
 */
class HTMLLink extends Column
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * HTMLLink constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\App\State $state
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\State $state,
        array $components = [],
        array $data = []

    )
    {
        $this->state = $state;
        $this->productRepository = $productRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (!isset($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as & $item) {
            $productUrls = [];
            /**
             * @todo: take a look at performance!
             *
             * We'll want to NOT get product twice, but perhaps
             * determine the product's url's by direct attribute getter.
             */
            $product = $this->productRepository->getById($item['entity_id']);
            $productData = $product->getData();

            if ($productData['status'] == 1 && $productData['visibility'] != 1) {
                $storeIds = $product->getStoreIds();
                if (!empty($storeIds)) {
                    foreach ($storeIds as $storeId) {
                        $product->setStoreId($storeId);
                        $productUrl = $this->state->emulateAreaCode(
                            \Magento\Framework\App\Area::AREA_FRONTEND,
                            [
                                $this,
                                'getStoreUrl'
                            ],
                            [
                                $storeId,
                                $item['entity_id']
                            ]
                        );

                        $productUrls[] = '<a target="_blank"  href="' . $productUrl . '">' . __($product->getStore($storeId)->getWebsite()->getName() . '(' . $storeId . ')') . '</a>';
                        $item[$this->getData('name')] = implode(', ', $productUrls);
                    }
                }
            } else {
                $item[$this->getData('name')] = 'No product link available';
            }
        }
        return $dataSource;
    }

    /**
     * Get store specific product url
     *
     * @param $storeId
     * @param $productId
     * @return mixed
     */
    public function getStoreUrl($storeId, $productId)
    {
        $product = $this->productRepository->getById($productId, false, $storeId);
        return $product->setStoreId($storeId)
            ->getUrlModel()
            ->getUrlInStore($product, ['_escape' => true]);
    }
}
